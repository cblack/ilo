package app

import (
	"os"
	"strings"

	"github.com/urfave/cli"
)

func remove(c *cli.Context) error {
	if c.Args().First() == "" {
		println("Error: provide an install target")
		os.Exit(1)
	}
	name := strings.Split(c.Args().First(), ":")
	if len(name) < 2 {
		println("Error: provide a valid name in the form of repo:package")
		os.Exit(1)
	}
	pkg := locatePackage(name[0], name[1])
	if pkg == nil {
		println("Error: failed to find package", c.Args().First())
		os.Exit(1)
	}
	println(pkg.toString())
	if c.GlobalBool("assume-yes") || boolAsk("Is this the package you want to remove?") {
		neededBy := append(pkg.dependentRepos(), *pkg)
		neededByInstalled := []Repo{}
		for _, pkg := range neededBy {
			if pkg.installed() {
				neededByInstalled = append(neededByInstalled, pkg)
			}
		}
		if len(neededByInstalled) == 0 {
			println("\nAll packages are already removed. Nothing to do.")
			os.Exit(0)
		}
		var strs []string
		for _, pkg := range neededByInstalled {
			strs = append(strs, pkg.toColorRN())
		}
		println()
		println(strings.Join(strs, " "), "\n")
		if c.GlobalBool("assume-yes") || boolAsk("These packages will be removed. Continue?") {
			for _, pkg := range neededByInstalled {
				pkg.remove()
			}
		}
	}
	return nil
}
