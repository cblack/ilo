package app

import (
	"fmt"
	"log"
	"os"
	"os/exec"
	"path"
	"path/filepath"
	"strconv"
	"strings"
	"syscall"

	"github.com/urfave/cli"
)

func mkdir(dir string) string {
	os.MkdirAll(path.Join(iloDir.overlayDir(), dir), os.ModePerm)
	return path.Join(iloDir.overlayDir(), dir)
}

func mount(root string, lower []string, upper string, workdir string) {
	lowers := append([]string{root}, lower...)
	cmd := exec.Command("fuse-overlayfs", "-o", fmt.Sprintf("lowerdir=%s,upperdir=%s,workdir=%s", strings.Join(lowers, ":"), mkdir(upper), mkdir(workdir)), root)
	err := cmd.Run()
	if err != nil {
		log.Fatalf("Error when running fuse-overlayfs: %s\n", err)
	}
	if cmd.ProcessState.ExitCode() != 0 {
		log.Fatalf("Exit code for fuse-overlayfs is nonzero: %d\n", cmd.ProcessState.ExitCode())
	}
}

func grabPackageDirs(loc string) []string {
	var ret []string
	strs, _ := filepath.Glob(path.Join(iloDir.installDir(), "*", "*", loc))
	for _, str := range strs {
		fi, err := os.Stat(str)
		if err != nil {
			continue
		}
		if fi.Mode().IsDir() {
			ret = append(ret, str)
		}
	}
	return ret
}

func nonEnterOverlay(c *cli.Context) error {
	mountPath := path.Join(iloDir.overlayDir(), "mount")
	os.MkdirAll(mountPath, os.ModePerm)
	mount(mountPath, append([]string{"/"}, grabPackageDirs("")...), "upper", ".workdir")
	println("Mounted at", mountPath)
	println("Use fusermount3 -u to unmount")
	return nil
}

func runOverlaid(command []string, print bool, env ...string) {
	cmd := exec.Command(iloBinPath, overlayCommand, "-u", strconv.Itoa(os.Getuid()), "-g", strconv.Itoa(os.Getgid()), "-e", strings.Join(command, "1<|>1"))

	cmd.Stdin = os.Stdin
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	cmd.Env = append(os.Environ(), env...)
	cmd.Env = append(cmd.Env, "ILO_OVERLAID=true")

	if print {
		println(cmd.Path, strings.Join(cmd.Args, " "))
	}

	uidmapping := []syscall.SysProcIDMap{
		{
			ContainerID: 0,
			HostID:      os.Getuid(),
			Size:        1,
		},
	}

	groupmapping := []syscall.SysProcIDMap{
		{
			ContainerID: 0,
			HostID:      os.Getgid(),
			Size:        1,
		},
	}

	cmd.SysProcAttr = &syscall.SysProcAttr{
		Cloneflags:  syscall.CLONE_NEWUSER | syscall.CLONE_NEWNS,
		UidMappings: uidmapping,
		GidMappings: groupmapping,
	}

	if err := cmd.Run(); err != nil {
		fmt.Printf("Error starting the exec.Command - %s\n", err)
		os.Exit(1)
	}
}

func overlay(c *cli.Context) error {
	runOverlaid(c.Args(), false)
	return nil
}

func internalOverlay(c *cli.Context) error {
	uid, gid := c.Int("u"), c.Int("g")
	toExec := strings.Split(c.String("e"), "1<|>1")

	if len(toExec) == 0 || (len(toExec) == 1 && toExec[0] == "") {
		log.Fatal("No command passed")
	}

	mount("/usr", grabPackageDirs("/usr"), "upper", ".workdir")
	mount("/etc", grabPackageDirs("/etc"), "upper", ".workdir")

	var cmd *exec.Cmd

	if len(toExec) >= 2 {
		cmd = exec.Command(toExec[0], toExec[1:]...)
	} else {
		cmd = exec.Command(toExec[0])
	}

	cmd.Stdin = os.Stdin
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr

	uidmapping := []syscall.SysProcIDMap{
		{
			ContainerID: uid,
			HostID:      os.Getuid(),
			Size:        1,
		},
	}

	groupmapping := []syscall.SysProcIDMap{
		{
			ContainerID: gid,
			HostID:      os.Getgid(),
			Size:        1,
		},
	}

	cmd.SysProcAttr = &syscall.SysProcAttr{
		Unshareflags: syscall.CLONE_NEWUSER,
		UidMappings:  uidmapping,
		GidMappings:  groupmapping,
	}

	if err := cmd.Run(); err != nil {
		fmt.Printf("Error starting the exec.Command - %s\n", err)
		os.Exit(1)
	}
	return nil
}
