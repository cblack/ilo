package app

import (
	"fmt"

	. "github.com/logrusorgru/aurora"
	"github.com/urfave/cli"
)

func installed(c *cli.Context) error {
	println("The following packages are installed:")
	for _, collection := range collections {
		println(Bold(kule(collection.Name)).String())
		for _, pkg := range collection.Repo {
			if pkg.installed() {
				fmt.Printf(" | %s\t\t%s\n", Bold(pkg.Name), pkg.Description)
			}
		}
	}
	return nil
}
