package app

import (
	"encoding/json"
	"fmt"
	"hash/fnv"
	"log"

	. "github.com/logrusorgru/aurora"
)

func handleErr(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

func printStruct(v interface{}) {
	dat, err := json.MarshalIndent(v, "", "    ")
	handleErr(err)
	fmt.Println(string(dat))
}

func kule(s string) string {
	h := fnv.New32a()
	h.Write([]byte(s))
	sum := h.Sum32()
	color := sum % 6
	switch color {
	case 0:
		return Red(s).String()
	case 1:
		return Green(s).String()
	case 2:
		return Yellow(s).String()
	case 3:
		return Blue(s).String()
	case 4:
		return Magenta(s).String()
	case 5:
		return Cyan(s).String()
	}
	return Blue(s).String()
}
