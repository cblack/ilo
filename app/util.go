package app

import (
	"fmt"
	"os"
	"path"
	"strings"

	"github.com/gofrs/flock"
	"github.com/manifoldco/promptui"
)

type confString string

type iloString string

var confDir confString

var iloDir iloString

var iloBinPath string

var collections []Module

var installedLock *flock.Flock
var repoLock *flock.Flock

func locatePackage(repo, name string) *Repo {
	for _, collection := range collections {
		for _, pkg := range collection.Repo {
			if repo == collection.Name && pkg.Name == name {
				return &pkg
			}
		}
	}
	return nil
}

func (conf confString) collectionLocation() string {
	return path.Join(string(conf), "collections")
}

func (conf confString) installedLock() string {
	return path.Join(string(conf), "db.lock")
}

func (conf confString) repoLock() string {
	return path.Join(string(conf), "repo.lock")
}

func (conf confString) mkdir() {
	os.MkdirAll(confDir.collectionLocation(), os.ModePerm)
}

func (ilo iloString) srcDir() string {
	return path.Join(string(ilo), "src")
}

func (ilo iloString) buildDir() string {
	return path.Join(string(ilo), "build")
}

func (ilo iloString) installDir() string {
	return path.Join(string(ilo), "packages")
}

func (ilo iloString) overlayDir() string {
	return path.Join(string(ilo), "overlay")
}

func (ilo iloString) mkdir() {
	os.MkdirAll(ilo.srcDir(), os.ModePerm)
	os.MkdirAll(ilo.buildDir(), os.ModePerm)
	os.MkdirAll(ilo.installDir(), os.ModePerm)
	os.MkdirAll(ilo.overlayDir(), os.ModePerm)
}

func boolAsk(prompt string) bool {
	promptObj := promptui.Select{
		Label:        prompt,
		Items:        []string{"Yes", "No"},
		HideSelected: true,
	}
	_, result, err := promptObj.Run()
	handleErr(err)
	return result == "Yes"
}

func waitLock(f *flock.Flock) {
	locked, err := f.TryLock()
	handleErr(err)
	if !locked {
		println("Waiting on lock...")
		f.Lock()
	}
}

func run(cmd string, args ...string) {
	runOverlaid(append([]string{cmd}, args...), true)
}

func destdir(destdir, cmd string, args ...string) {
	runOverlaid(append([]string{cmd}, args...), true, fmt.Sprintf("DESTDIR=%s", destdir))
}

func contains(one, two string) bool {
	return strings.Contains(strings.ToLower(one), strings.ToLower(two))
}

func init() {
	if os.Getenv("ILO_OVERLAID") != "true" {
		config, err := os.UserConfigDir()
		if err == nil {
			confDir = confString(path.Join(config, "ilo"))
		} else {
			panic(err)
		}
		home, err := os.UserHomeDir()
		if err == nil {
			iloDir = iloString(path.Join(home, "ilo"))
		} else {
			panic(err)
		}
		ex, err := os.Executable()
		if err == nil {
			iloBinPath = ex
		} else {
			panic(err)
		}
		confDir.mkdir()
		installedLock = flock.New(confDir.installedLock())
		repoLock = flock.New(confDir.repoLock())
		loadCollections()
	}
}
