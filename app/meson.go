package app

import (
	"os"
)

func mesonConfigure(r Repo) error {
	cwd, err := os.Getwd()
	if err != nil {
		return err
	}
	defer os.Chdir(cwd)
	err = os.Chdir(r.srcPath())
	if err != nil {
		return err
	}
	run("meson", r.buildPath(), "--prefix=/usr")
	return nil
}

func mesonBuild(r Repo) error {
	cwd, err := os.Getwd()
	if err != nil {
		return err
	}
	defer os.Chdir(cwd)
	err = os.Chdir(r.buildPath())
	if err != nil {
		return err
	}
	run("ninja", "-j8")
	destdir(r.installPath(), "ninja", "install")
	return nil
}
