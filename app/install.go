package app

import (
	"fmt"
	"os"
	"strings"

	"github.com/urfave/cli"
)

type depTree struct {
	Pkg      Repo
	Children []*depTree
}

func (t *depTree) fill(shouldErr bool) {
	for _, dep := range t.Pkg.Depends {
		name := strings.Split(dep, ":")
		if len(name) < 2 {
			println("Error: dependency is not in the form repo:package")
			os.Exit(1)
		}
		pkg := locatePackage(name[0], name[1])
		if pkg == nil {
			if shouldErr {
				println("Error: package", name[0]+":"+name[1], "not found in repos")
				os.Exit(1)
			} else {
				return
			}
		}
		sub := depTree{
			Pkg: *pkg,
		}
		sub.fill(shouldErr)
		t.Children = append(t.Children, &sub)
	}
}

func (t *depTree) print(i int) {
	indent := strings.Repeat("    ", i)
	fmt.Printf("%s%s:%s\n", indent, t.Pkg.Module, t.Pkg.Name)
	for _, child := range t.Children {
		child.print(i + 1)
	}
}

func (t *depTree) trimLeaves(resolved *[]Repo) {
	var childrenWithLeaves []*depTree
	for _, tree := range t.Children {
		if len(tree.Children) > 0 {
			childrenWithLeaves = append(childrenWithLeaves, tree)
		} else {
			toAppend := true
			for _, pkg := range *resolved {
				if pkg.Name == tree.Pkg.Name && pkg.Module == tree.Pkg.Module {
					toAppend = false
				}
			}
			if toAppend {
				*resolved = append(*resolved, tree.Pkg)
			}
		}
	}
	t.Children = childrenWithLeaves
	for _, child := range t.Children {
		child.trimLeaves(resolved)
	}
}

func (t *depTree) keepTrimming(resolved *[]Repo) {
	for len(t.Children) > 0 {
		t.trimLeaves(resolved)
	}
	*resolved = append(*resolved, t.Pkg)
}

func (t *depTree) hasLeaf(r Repo) bool {
	if t.Pkg.toRN() == r.toRN() {
		return true
	}
	for _, tree := range t.Children {
		hasLeaf := tree.hasLeaf(r)
		if hasLeaf {
			return true
		}
	}
	return false
}

func resolvePackage(pkg *Repo) []Repo {
	tree := depTree{
		Pkg: *pkg,
	}
	tree.fill(true)
	var resolved []Repo
	tree.keepTrimming(&resolved)
	return resolved
}

func install(c *cli.Context) error {
	if c.Args().First() == "" {
		println("Error: provide an install target")
		os.Exit(1)
	}
	name := strings.Split(c.Args().First(), ":")
	if len(name) < 2 {
		println("Error: provide a valid name in the form of repo:package")
		os.Exit(1)
	}
	pkg := locatePackage(name[0], name[1])
	if pkg == nil {
		println("Error: failed to find package", c.Args().First())
		os.Exit(1)
	}
	println(pkg.toString())
	if c.GlobalBool("assume-yes") || boolAsk("Is this the package you want to install?") {
		pkgs := resolvePackage(pkg)
		var filtered []Repo
		for _, pkg := range pkgs {
			if !pkg.installed() {
				filtered = append(filtered, pkg)
			}
		}
		if len(filtered) == 0 {
			println("\nAll packages are already installed. Nothing to do.")
			os.Exit(0)
		}
		var strs []string
		for _, pkg := range filtered {
			strs = append(strs, pkg.toColorRN())
		}
		println()
		println(strings.Join(strs, " "), "\n")
		if c.GlobalBool("assume-yes") || boolAsk("These packages will be installed. Continue?") {
			for _, pkg := range filtered {
				pkg.install()
			}
		}
	}
	return nil
}
