package app

import (
	"strings"

	"github.com/urfave/cli"
)

func update(c *cli.Context) error {
	virt := Repo{}
	for _, collection := range collections {
		for _, pkg := range collection.Repo {
			if pkg.installed() {
				virt.Depends = append(virt.Depends, pkg.toRN())
			}
		}
	}
	solved := resolvePackage(&virt)
	if len(solved) < 2 {
		println("No packages are installed.")
		return nil
	}
	var strs []string
	for _, pkg := range solved[:len(solved)-1] {
		strs = append(strs, pkg.toColorRN())
	}
	println()
	println(strings.Join(strs, " "), "\n")
	if c.GlobalBool("assume-yes") || boolAsk("These packages will be installed. Continue?") {
		for _, pkg := range solved[:len(solved)-1] {
			pkg.update()
		}
	}
	return nil
}
