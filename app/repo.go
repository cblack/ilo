package app

import (
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"path"

	"github.com/BurntSushi/toml"
	"github.com/go-git/go-git/v5"
	"github.com/go-git/go-git/v5/plumbing"
	. "github.com/logrusorgru/aurora"
)

type Repo struct {
	// Things that don't inherit.
	Name        string
	Description string
	URL         string
	NotDepends  []string
	// Things that inherit.
	Depends              []string
	BuildSystem          string
	VersionControlSystem string
	Tag                  string
	Branch               string
	Version              string
	Module               string
}

type Module struct {
	// Things that don't inherit.
	Name        string
	Description string `toml:"Description"`
	Version     string
	// Things that inherit.
	Depends              []string
	BuildSystem          string
	VersionControlSystem string `toml:"vcs"`
	Branch               string
	Tag                  string
	// Child repos.
	Repo []Repo
}

type Collection struct {
	Module []Module `toml:"module"`
}

func loadCollections() {
	files, err := ioutil.ReadDir(confDir.collectionLocation())
	handleErr(err)
	for _, file := range files {
		var collection Collection
		_, err := toml.DecodeFile(path.Join(confDir.collectionLocation(), file.Name()), &collection)
		handleErr(err)
		for _, module := range collection.Module {
			for key, repo := range module.Repo {
				repo.Depends = append(repo.Depends, module.Depends...)
				b := repo.Depends[:0]
				for _, dep := range repo.Depends {
					include := true
					for _, antiDep := range repo.NotDepends {
						if dep == antiDep {
							include = false
							break
						}
					}
					if include {
						b = append(b, dep)
					}
				}
				repo.Depends = b
				if repo.BuildSystem == "" {
					repo.BuildSystem = module.BuildSystem
				}
				if repo.VersionControlSystem == "" {
					repo.VersionControlSystem = module.VersionControlSystem
				}
				if repo.Branch == "" {
					repo.Branch = module.Branch
				}
				if repo.Tag == "" {
					repo.Tag = module.Tag
				}
				if repo.Version == "" {
					repo.Version = module.Version
				}
				repo.Module = module.Name
				module.Repo[key] = repo
			}
			collections = append(collections, module)
		}
	}
}

func (r Repo) toString() string {
	return Sprintf("%s:%s\t%s\n\t%s", Bold(kule(r.Module)), Bold(r.Name), r.Version, r.Description)
}

func (r Repo) toColorRN() string {
	return Sprintf("%s:%s", Bold(kule(r.Module)), Bold(r.Name))
}

func (r Repo) toRN() string {
	return fmt.Sprintf("%s:%s", r.Module, r.Name)
}

func (r Repo) dependentRepos() []Repo {
	var repos []Repo
	for _, module := range collections {
		for _, repo := range module.Repo {
			tree := depTree{
				Pkg: repo,
			}
			tree.fill(false)
			if tree.hasLeaf(r) {
				if repo.toRN() != r.toRN() {
					repos = append(repos, repo)
				}
			}
		}
	}
	return repos
}

func (r Repo) srcPath() string {
	return path.Join(iloDir.srcDir(), r.Module, r.Name)
}

func (r Repo) buildPath() string {
	return path.Join(iloDir.buildDir(), r.Module, r.Name)
}

func (r Repo) installPath() string {
	return path.Join(iloDir.installDir(), r.Module, r.Name)
}

func (r Repo) fetch() error {
	os.MkdirAll(r.srcPath(), os.ModePerm)
	if r.VersionControlSystem == "git" {
		fmt.Printf("Cloning %s...\n", r.toColorRN())
		opts := git.CloneOptions{
			URL:               r.URL,
			Depth:             1,
			RecurseSubmodules: git.DefaultSubmoduleRecursionDepth,
			SingleBranch:      true,
			Progress:          os.Stdout,
		}
		if r.Branch != "" {
			opts.ReferenceName = plumbing.NewBranchReferenceName(r.Branch)
		}
		if r.Tag != "" {
			opts.ReferenceName = plumbing.NewTagReferenceName(r.Tag)
		}
		_, err := git.PlainClone(r.srcPath(), false, &opts)
		if err == git.ErrRepositoryAlreadyExists {
			repo, err := git.PlainOpen(r.srcPath())
			if err != nil {
				return err
			}
			worktree, err := repo.Worktree()
			if err != nil {
				return err
			}
			worktree.Pull(&git.PullOptions{RemoteName: "origin"})
		} else if err != nil {
			return err
		}
		return nil
	}
	return errors.New(Sprintf("VCS %s not known", Blue(r.VersionControlSystem)))
}

func (r Repo) configure() error {
	os.MkdirAll(r.buildPath(), os.ModePerm)
	switch r.BuildSystem {
	case "meson":
		return mesonConfigure(r)
	case "qt-custom":
		return qtCustomConfigure(r)
	case "qmake":
		return qmakeConfigure(r)
	}
	return errors.New(Sprintf("Build system %s not known", Blue(r.BuildSystem)))
}

func (r Repo) build() error {
	os.MkdirAll(r.buildPath(), os.ModePerm)
	switch r.BuildSystem {
	case "meson":
		return mesonBuild(r)
	case "qt-custom":
		return qtCustomBuild(r)
	case "qmake":
		return qmakeBuild(r)
	}
	return errors.New(Sprintf("Build system %s not known", Blue(r.BuildSystem)))
}

func (r Repo) installed() bool {
	_, err := os.Stat(r.installPath())
	return !os.IsNotExist(err)
}

func (r Repo) remove() {
	if r.installed() {
		os.RemoveAll(r.installPath())
	}
}

func (r Repo) install() {
	if r.installed() {
		return
	}
	handleErr(r.fetch())
	handleErr(r.configure())
	handleErr(r.build())
}

func (r Repo) update() {
	handleErr(r.fetch())
	handleErr(r.build())
}
