package app

import (
	"errors"
	"fmt"
	"os"
	"path"
	"path/filepath"
)

const libqt5_prefix = "/usr"
const libqt5_libdir = "/usr/lib64"
const libqt5_archdatadir = "/usr/lib64/qt5"
const libqt5_bindir = "/usr/lib64/qt5/bin"
const libqt5_datadir = "/usr/share/qt5"
const libqt5_docdir = "/usr/doc/qt5"
const libqt5_examplesdir = "/usr/lib64/qt5/examples"
const libqt5_includedir = "/usr/include/qt5"
const libqt5_importdir = "/usr/lib64/qt5/imports"
const libqt5_libexecdir = "/usr/lib64/qt5/libexec"
const libqt5_plugindir = "/usr/lib64/qt5/plugins"
const libqt5_sysconfdir = "/etc/xdg"
const libqt5_translationdir = "/usr/lib64/qt5/translations"
const libqt5_headerdir = "/usr/include/qt5"

func qtCustomConfigure(r Repo) error {
	cwd, err := os.Getwd()
	if err != nil {
		return err
	}
	defer os.Chdir(cwd)
	err = os.Chdir(r.buildPath())
	if err != nil {
		return err
	}
	run(
		path.Join(r.srcPath(), "configure"),
		"-prefix", libqt5_prefix,
		"-L", libqt5_libdir,
		"-archdatadir", libqt5_archdatadir,
		"-bindir", libqt5_bindir,
		"-libdir", libqt5_libdir,
		"-libexecdir", libqt5_libexecdir,
		"-datadir", libqt5_datadir,
		"-docdir", libqt5_docdir,
		"-examplesdir", libqt5_examplesdir,
		"-headerdir", libqt5_headerdir,
		"-importdir", libqt5_importdir,
		"-plugindir", libqt5_plugindir,
		"-sysconfdir", libqt5_sysconfdir,
		"-translationdir", libqt5_translationdir,
		"-release",
		"-shared",
		"-accessibility",
		"-fontconfig",
		"-glib",
		"-gtk",
		"-icu",
		"-optimized-qmake",
		"-dbus-runtime",
		"-nomake", "examples",
		"-nomake", "tests",
		"-opensource",
		"-no-pch",
		"-no-rpath",
		"-no-strip",
		"-confirm-license",
	)
	return nil
}

func qtCustomBuild(r Repo) error {
	cwd, err := os.Getwd()
	if err != nil {
		return err
	}
	defer os.Chdir(cwd)
	err = os.Chdir(r.buildPath())
	if err != nil {
		return err
	}
	run(
		"make",
		"-j8",
		"-C",
		"qmake",
		"all",
		"binary",
	)
	run(
		"make",
		"-j8",
	)
	run(
		"make",
		"install",
		fmt.Sprintf("INSTALL_ROOT=%s", r.installPath()),
	)
	return nil
}

func qmakeConfigure(r Repo) error {
	cwd, err := os.Getwd()
	if err != nil {
		return err
	}
	defer os.Chdir(cwd)
	err = os.Chdir(r.buildPath())
	if err != nil {
		return err
	}
	matches, err := filepath.Glob(path.Join(r.srcPath(), "*.pro"))
	if err != nil {
		return err
	}
	if len(matches) < 1 {
		return errors.New("Could not find .pro file in " + r.srcPath())
	}
	run(
		"/usr/lib64/qt5/bin/qmake",
		matches[0],
	)
	return nil
}

func qmakeBuild(r Repo) error {
	cwd, err := os.Getwd()
	if err != nil {
		return err
	}
	defer os.Chdir(cwd)
	err = os.Chdir(r.buildPath())
	if err != nil {
		return err
	}
	run(
		"make",
		"-j8",
	)
	run(
		"make",
		"install",
		fmt.Sprintf("INSTALL_ROOT=%s", r.installPath()),
	)
	return nil
}
