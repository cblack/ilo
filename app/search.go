package app

import "github.com/urfave/cli"

func search(c *cli.Context) error {
	args := c.Args()
	if len(args) == 0 {
		for _, collection := range collections {
			for _, repo := range collection.Repo {
				println(repo.toString())
			}
		}
	}
	set := make(map[string]Repo)
	for _, arg := range args {
		for _, collection := range collections {
			for _, repo := range collection.Repo {
				if contains(repo.Name, arg) {
					set[collection.Name+":"+repo.Name] = repo
				}
				if contains(repo.Description, arg) {
					set[collection.Name+":"+repo.Name] = repo
				}
				if contains(collection.Name, arg) {
					set[collection.Name+":"+repo.Name] = repo
				}
			}
		}
	}
	for _, repo := range set {
		println(repo.toString())
	}
	return nil
}
