package app

import (
	"fmt"
	"os"
	"path"

	"github.com/urfave/cli"
)

const overlayCommand = "c̶m҉d̨"

func Main() {
	app := &cli.App{
		Name:  "ilo",
		Usage: "Manage your local projects",
		Flags: []cli.Flag{
			cli.BoolFlag{
				Name:  "assume-yes",
				Usage: "Assume yes for any options",
			},
		},
		Commands: []cli.Command{
			{
				Name:    "search",
				Aliases: []string{"se"},
				Usage:   "Search through collections",
				Action:  search,
			},
			{
				Name:    "install",
				Aliases: []string{"in"},
				Usage:   "Install a package",
				Action:  install,
			},
			{
				Name:    "remove",
				Aliases: []string{"rm"},
				Usage:   "Remove a package",
				Action:  remove,
			},
			{
				Name:    "enter",
				Aliases: []string{"en"},
				Usage:   "Enter the ilo environment",
				Action:  overlay,
			},
			{
				Name:   "installed",
				Usage:  "List installed packages.",
				Action: installed,
			},
			{
				Name:    "conjure",
				Aliases: []string{"mount", "summon"},
				Usage:   "Mount the ilo environment at ~/ilo/overlay/mount",
				Action:  nonEnterOverlay,
			},
			{
				Name:    "update",
				Aliases: []string{"up", "dup"},
				Usage:   "Update your packages",
				Action:  update,
			},
			{
				Name:    "refresh",
				Aliases: []string{"refresh"},
				Usage:   "Refresh known packages",
				Action: func(_ *cli.Context) error {
					for _, str := range []string{"Dummy", "GNOME", "Qt5"} {
						os.Remove(path.Join(confDir.collectionLocation(), fmt.Sprintf("%s.collection.toml", str)))
						run("wget", "--backups=1", fmt.Sprintf("https://invent.kde.org/cblack/ilo/-/raw/master/%s.collection.toml", str), "-P", confDir.collectionLocation())
					}
					return nil
				},
			},
			{
				Name:   overlayCommand,
				Action: internalOverlay,
				Hidden: true,
				Flags: []cli.Flag{
					&cli.IntFlag{
						Name:  "u",
						Value: -1,
					},
					&cli.IntFlag{
						Name:  "g",
						Value: -1,
					},
					&cli.StringFlag{
						Name:  "e",
						Value: "",
					},
				},
			},
		},
	}
	app.Run(os.Args)
}
