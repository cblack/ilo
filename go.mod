module invent.kde.org/cblack/ilo

go 1.14

require (
	github.com/BurntSushi/toml v0.3.1
	github.com/go-git/go-git/v5 v5.0.0
	github.com/gofrs/flock v0.7.1
	github.com/logrusorgru/aurora v0.0.0-20200102142835-e9ef32dff381
	github.com/manifoldco/promptui v0.7.0
	github.com/mattn/go-colorable v0.1.6 // indirect
	github.com/urfave/cli v1.22.4
	golang.org/x/sys v0.0.0-20200413165638-669c56c373c4 // indirect
)
